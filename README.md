# BCTHS Walkout Video

This is the GitLab repo for the BCTHS Walkout. It contains all the footage I could obtain for the relating to the BCTHS Walkout. It also contains work for Video Editing relating to the walkout. Anyone who wants to use these files to edit their own videos is free to do so as long as they follow all the conditions of the attached license. 

## Contributing
If you want, you can also make your own branch and help me out with editing. All of the Video Editing Files are done in Blender and the audio is done in Audacity. Both of these programs are free and cross-platform so everyone should be able work with them. You can also make suggestions in the Issues section.

Thank you!

P.S. Please Fork